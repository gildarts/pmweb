import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { LoginComponent } from './credential/login/login.component';
import { AuthGuardService } from './core/auth-guard.service';
import { RootComponent } from './root.component';

const routes: Routes = [
  { path: '', component: RootComponent },
  { path: 'login', component: LoginComponent },
  { path: 'uds', loadChildren: './uds/uds.module#UdsModule', canActivate: [AuthGuardService] },
  { path: 'udt', loadChildren: './udt/udt.module#UdtModule', canActivate: [AuthGuardService] },
  { path: 'ischool', loadChildren: './ischool/ischool.module#IschoolModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

