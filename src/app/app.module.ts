import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { CredentialModule } from './credential/credential.module';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { RootComponent } from './root.component';
import { ContentComponent } from './content.component';
import { DSAModule } from './dsa';

@NgModule({
  declarations: [
    AppComponent,
    RootComponent,
    ContentComponent,
  ],
  imports: [
    BrowserModule,
    CoreModule,
    SharedModule,
    CredentialModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    DSAModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
