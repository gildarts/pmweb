import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DSAService } from './dsa';
import { DSARegistration } from './dsa/dsa-registration';
import { startWith } from 'rxjs/operators';
import { Router } from '@angular/router';
import { WindowRefService } from './core/window-ref.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ContentComponent implements OnInit {

  displayedColumns = ['name', 'dsns', 'action'];
  schools: DSARegistration[];

  constructor(
    private router: Router,
    private dsa: DSAService
  ) {
  }

  async ngOnInit() {
    this.dsa.registerChanged.pipe(
      startWith(0)
    ).subscribe(async () => {
        await this.refreshSchools();
      });
  }

  private async refreshSchools() {
    this.schools = await this.dsa.list().toPromise();
  }

  openUdtWindow(ds: DSARegistration) {
    this.router.navigate(['/udt', ds.dsns]);
  }

  async editRegistration(ds: DSARegistration) {
    const reg = await this.dsa.register(ds).toPromise();

    if (reg) {
      const rsp = await this.dsa.saveRegistration(reg).toPromise();
    } else {
      console.log('nothing...');
    }  }
}
