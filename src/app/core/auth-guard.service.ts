import { Injectable } from '@angular/core';
import { CoreModule } from './core.module';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: CoreModule
})
export class AuthGuardService implements CanActivate {

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.auth.authenticated.pipe(
      tap(v => {
        if (!v) {
          console.warn('登入守衛發動!!');
          this.router.navigate(['/login']);
        }
      })
    );
  }
}
