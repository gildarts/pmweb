import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of, throwError, Observable } from 'rxjs';
import { mapTo, switchMap, tap, catchError, shareReplay } from 'rxjs/operators';
import { GoogleUserData } from './google-user-data';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private $userinfo: Observable<GoogleUserData>;

  private $loginSuccess: Observable<boolean>;

  constructor(
    private http: HttpClient
  ) {
    this.initObservables();
  }

  /**
   *是否已經登入完成。
   *
   * @returns 是否已登入。
   * @memberof AuthService
   */
  public get authenticated() {
    return this.$loginSuccess;
  }

  /**
   *取得使用者資訊
   *
   * @readonly
   * @memberof AuthService
   */
  public get userInfo() {
    return this.$userinfo;
  }

  /**
   *清除登入狀態快取。
   *
   * @memberof AuthService
   */
  public reset() {
    this.initObservables();
  }

  /**
   * 登出系統。
   *
   * @memberof AuthService
   */
  public logout() {
    return this.http.get('api/session/clean').pipe(
      switchMap(rsp => {
        const { success } = rsp as { success: boolean; };
        if (success) {
          return of(true);
        } else {
          return throwError({ error: '「api/session/clean」response not correctly.' });
        }
      }),
      tap(() => {
        this.reset(); // 重置登入狀態快取。
      })
    );
  }

  /**
   *建立登入檢查相關流程，並且只會執行一次 service 呼叫。
   *
   * @private
   * @memberof AuthService
   */
  private initObservables() {
    this.$userinfo = this.http.get<GoogleUserData>('api/auth/userinfo').pipe(
      shareReplay(1) // 只會呼叫一次，不管多少 subscribers。
    );

    this.$loginSuccess = this.http.get('api/auth/available').pipe(
      switchMap(rsp => {
        const { success } = rsp as { success: boolean; };

        if (success) { // 依結果決定是否呼叫 get userinfo service。
          return this.$userinfo;
        } else {
          return throwError({ error: '「api/auth/available」response not correctly.' });
        }
      }),
      mapTo(true), // 能到這裡就一定是 true 了。
      catchError((err: HttpErrorResponse) => {
        console.error(err.error);
        return of(false);
      }),
      shareReplay(1) // 防止重複呼叫 service。
    );
  }
}
