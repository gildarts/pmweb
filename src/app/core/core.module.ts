import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [],
  declarations: []
})
export class CoreModule {
  constructor(
    @Optional() @SkipSelf() parentModule: CoreModule
  ) {
    // 防止 CoreModule 被其他模組不小心 import。
    if (parentModule) {
      throw new Error(`「CoreModule」 has already been loaded. Import Core modules in the AppModule only.`);
    }
  }

}
