import { Connection } from './utils/Connection';

export interface DSARegistration {
  id?: number;

  dsns: string;

  login_user: string;

  login_password: string;

  /**
   * 通常是中文名稱。
   */
  name?: string;

  description?: string;

  /**
   * 順便提供，不一定有。
   */
  connection?: Connection;
}
