import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DSNSChooserComponent } from '../dsns-chooser/dsns-chooser.component';
import { map, switchMap, distinctUntilChanged, debounceTime, tap } from 'rxjs/operators';
import { SchoolRecord, SchoolService } from '../school.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { DialogService } from '../../shared/dialog.service';
import { Connection } from '../utils/Connection';
import { HttpClient } from '@angular/common/http';
import { DSNSService, DSNSRecord } from '../dsns.service';
import { DSARegistration } from '../dsa-registration';
import { MatAutocompleteSelectedEvent } from '@angular/material';

@Component({
  selector: 'app-dsa-registration',
  templateUrl: './dsa-registration.component.html',
  styleUrls: ['./dsa-registration.component.css']
})
export class DSARegistrationComponent implements OnInit {

  registration: FormGroup;

  dsnsCompleteSource: SchoolRecord[] = [];

  dsnsLoading = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: DSARegistration,
    private dialogRef: MatDialogRef<DSARegistrationComponent>,
    private dialogSrv: DialogService,
    private dsns: DSNSService,
    private school: SchoolService,
    private http: HttpClient,
    fb: FormBuilder
  ) {

    this.registration = fb.group({
      dsns: fb.control({dsns: data.dsns || ''}, Validators.required),
      user: fb.control('', Validators.required),
      password: fb.control('', Validators.required),
      name: fb.control(data.name || ''),
      description: fb.control(data.description || '')
    });
  }

  ngOnInit() {
    const { valueChanges: dsnsValues } = this.registration.get('dsns');
    dsnsValues.pipe(
      distinctUntilChanged(),
      debounceTime(700),
      tap(() => this.dsnsLoading = true),
      switchMap(v => this.dsns.getTop10(v)),
      map(v => v.map(dsns => dsns.name)),
      switchMap(v => this.school.getSchoolsInfo(v))
    ).
    subscribe(dsnsList => {
      this.dsnsCompleteSource = dsnsList;
      this.dsnsLoading = false;
    });
  }

  resolveDSNSInfo(option: MatAutocompleteSelectedEvent) {
    const { value }: { value: SchoolRecord } = option.option;

    if (value) {
      this.registration.patchValue({
        name: value.title,
        description: `核心:${value.coreSystem || '未設定'}\n學制:${value.type || '未設定'}\n縣市:${value.county || '未設定'}`
      });
    }
  }

  async testConnection() {
    const { dsns: { dsns }, user, password, name, description } = this.registration.value;

    try {
      this.registration.disable();
      const dsnsInfo = await this.dsns.getAccessPointInfo(dsns).toPromise();

      const conn = new Connection(dsnsInfo.physicalUrl, 'admin', user, password);
      await conn.connect(this.http);

      this.dialogRef.close({
        dsns,
        login_user: user,
        login_password: password,
        name: name,
        description: description,
        connection: conn
      } as DSARegistration);

    } catch (error) {
      console.error(error);
      if (error.response) {
        this.dialogSrv.json(error.response);
      } else {
        this.dialogSrv.json({ message: error.message });
      }
    } finally {
      this.registration.enable();
    }
  }

  getName(dsns: SchoolRecord) {
    return dsns.dsns;
  }
}
