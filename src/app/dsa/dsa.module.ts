import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DSARegistrationComponent } from './dsa-registration/dsa-registration.component';
import { SharedModule } from '../shared/shared.module';
import { DSNSChooserComponent } from './dsns-chooser/dsns-chooser.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    DSARegistrationComponent,
    DSNSChooserComponent,
  ],
  exports: [
  ],
  entryComponents: [
    DSARegistrationComponent,
    DSNSChooserComponent
  ]
})
export class DSAModule { }
