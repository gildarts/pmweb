import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { DSARegistrationComponent } from './dsa-registration/dsa-registration.component';
import { DSARegistration } from './dsa-registration';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DSAService {

  private $registerChanged: Subject<number>;

  constructor(
    private dialog: MatDialog,
    private http: HttpClient
  ) {
    this.$registerChanged = new Subject<number>();
  }

  public register(defaultInfo?: DSARegistration) {
    const dig = this.dialog.open(DSARegistrationComponent, {
      data: defaultInfo
    });

    return dig.afterClosed().pipe(
      map((rsp) => {
        if (rsp) {
          return (rsp as DSARegistration);
        } else {
          return false;
        }
      })
    );
  }

  public saveRegistration(registration: DSARegistration) {
    return this.http.post<{ id: number }>('api/registerdsa/set', {
      ...registration,
      connection: undefined
    }).pipe(
      // 通知資料變更。
      tap((v) => this.$registerChanged.next(v.id))
    );
  }

  public delete(identity: { id?: number, dsns?: string }) {
    return this.http.post<{ id: number }>('api/registerdsa/delete', identity).pipe(
      // 通知資料變更。
      tap((v) => this.$registerChanged.next(v.id))
    );
  }

  public list() {
    return this.http.get<DSARegistration[]>('api/registerdsa/list');
  }

  public get(identity: {id?: number, dsns?: string}) {
    return this.http.post<DSARegistration>('api/registerdsa/get', identity);
  }

  public get registerChanged() {
    return this.$registerChanged.asObservable();
  }
}
