import { Component, OnInit, OnDestroy, Optional } from '@angular/core';
import { DSNSService } from '../dsns.service';
import { SchoolService, SchoolRecord } from '../school.service';
import { MatSelectionListChange, MatDialogRef } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import {
  switchMap, takeUntil, startWith,
  debounceTime, distinctUntilChanged, filter
} from 'rxjs/operators';

// 目前沒有用到這個 Component。
@Component({
  selector: 'app-dsns-chooser',
  templateUrl: './dsns-chooser.component.html',
  styleUrls: ['./dsns-chooser.component.css']
})
export class DSNSChooserComponent implements OnInit, OnDestroy {

  keyword: FormControl = new FormControl('');
  $bag = new Subject<void>();
  dataSource: SchoolRecord[];

  constructor(
    private dsns: DSNSService,
    private school: SchoolService,
    @Optional() private dialogRef: MatDialogRef<DSNSChooserComponent>
  ) { }

  ngOnInit() {

    this.keyword.valueChanges.pipe(
      takeUntil(this.$bag),
      filter(v => v), // 慮掉空白。
      debounceTime(700),
      distinctUntilChanged(),
      startWith('demo'),
      switchMap(kw => this.dsns.getTop10(kw)),
      switchMap(dsnses => {
        const names = dsnses.map(v => v.name);
        return this.school.getSchoolsInfo(names);
      })
    )
      .subscribe(v => {
        this.dataSource = v;
      });

  }

  ngOnDestroy() {
    this.$bag.next();
  }

  selected(event: MatSelectionListChange) {
    if (this.dialogRef) {
      this.dialogRef.close(event.option.value);
    } else {
      console.log(event.option.value);
    }

  }
}
