import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Connection, DSAError } from './utils/Connection';
import { Observable, from, of, throwError } from 'rxjs';
import { mapTo, switchMap, map } from 'rxjs/operators';
import { Converter } from './utils/Envelope';

@Injectable({
  providedIn: 'root'
})
export class DSNSService {

  private conn: Observable<Connection>;

  constructor(
    private http: HttpClient
  ) {
    const c = new Connection('http://dsns.ischool.com.tw/dsns/dsns', '', '', '');
    c.enabledSession = false;

    this.conn = from(c.connect(http)).pipe(
      mapTo(c)
    );
  }

  /**
   *取得 DSA 資訊，包含 TLS 連接位置。
   *
   * @param {string} dsns要查詢的 DSNS。
   * @returns
   * @memberof DSNSService
   */
  public getAccessPointInfo(dsns: string) {
    const service = 'DS.NameService.GetAccessPointInfo';

    return this.conn.pipe(
      this.callService(service, { Name: dsns }),
      // 處理查不到的狀態，轉換成錯誤丟出。
      switchMap(v => {
        const info = v.Response.Application;
        return info ? of(info) : throwError(new DSAError(`「${dsns}」 not found.`));
      }),
      map(v => {
        return {
          name: v.Name,
          physicalUrl: v.PhysicalUrl,
          securedPhysicalUrl: v.SecuredPhysicalUrl,
        } as DSNSRecord;
      })
    );
  }

  /**
   *搜尋 DSNS 清單，不包含 TLS 連接位置。
   *
   * @param {string} query
   * @returns
   * @memberof DSNSService
   */
  public getTop10(query: string) {
    const service = 'DS.NameService.GetTop10';

    return this.conn.pipe(
      this.callService(service, { Name: query }),
      map(v => {
        const list = Converter.array(v, 'Response/Application');

        return list.map(dsns => {
          return {
            name: dsns.Name,
            physicalUrl: dsns.PhysicalUrl
          } as DSNSRecord;
        });
      })
    );
  }

  private callService(service, body) {
    return switchMap<Connection, any>((conn) => {
      const rsp = conn.send<any>(service, body);
      return from(rsp);
    });
  }

}

export interface DSNSRecord {
  name: string;

  physicalUrl: string;

  securedPhysicalUrl?: string;
}
