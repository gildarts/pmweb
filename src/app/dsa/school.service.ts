import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from, of } from 'rxjs';
import { Connection } from './utils/Connection';

import { mapTo, switchMap, map } from 'rxjs/operators';
import { Converter } from './utils/Envelope';

@Injectable({
  providedIn: 'root'
})
export class SchoolService {

  private conn: Observable<Connection>;

  constructor(
    private http: HttpClient
  ) {
    const dsns = 'http://dsns.ischool.com.tw/campusman.ischool.com.tw';
    const c = new Connection(dsns, 'config.public', '', '');

    this.conn = from(c.connect(http)).pipe(
      mapTo(c)
    );
  }

  /**
   *取得 DSNS 在管理中心的設定。
   *
   * @param {string[]} dsns
   * @returns
   * @memberof SchoolService
   */
  public getSchoolsInfo(dsns: string[]): Observable<SchoolRecord[]> {
    const service = 'GetSchoolList';

    if (dsns.length <= 0) {
      return of([]);
    }

    const req = {
      Request: {
        Field: {
          All: ''
        },
        Dsns: dsns
      }
    };

    return this.conn.pipe(
      this.callService<any>(service, req),
      map(v => {
        const list = Converter.array(v, 'Response/School');

        return list.map(school => {
          return {
            title: school.Title,
            dsns: school.DSNS,
            coreSystem: school.CoreSystem,
            county: school.County,
            type: school.Type
          } as SchoolRecord;
        });
      })
    );
  }

  private callService<T>(service, body) {
    return switchMap<Connection, T>((conn) => {
      const rsp = conn.send<T>(service, body);
      return from(rsp);
    });
  }
}

/**
 *代表一個學校 DSA。
 *
 * @export
 * @interface SchoolRecord
 */
export interface SchoolRecord {
  title: string;

  dsns: string;

  coreSystem: string;

  county: string;

  type: string;
}
