import { Envelope, Converter, SecurityToken } from './Envelope';
import { HttpClient } from '@angular/common/http';

export class Connection {

  private http: HttpClient;

  private sessionId: string;

  constructor(private dsns: string, private contract: string, private userid: string, private password: string) {
  }

  /**
   *是否啟用 session，預設為啟用。
   *
   * @memberof Connection
   */
  public enabledSession = true;

  /**
   * 進行 DSA 連線。
   */
  public async connect(http: HttpClient) {
    this.http = http;

    if (this.enabledSession) {
      const rsp: any = await this.send('DS.Base.Connect', { RequestSessionID: true });
      this.sessionId = rsp.SessionID;
    } else {
      await this.send('DS.Base.Connect');
    }

  }

  /**
   * 呼叫 Service。
   * @param serviceName 服務名稱。
   * @param bodyContent Request 內容，不含「Body」節點。
   */
  public async send<T>(serviceName: string, bodyContent?: any): Promise<T> {
    const { http } = this;
    const env: Envelope = this.createRequest(this.contract, serviceName, bodyContent);
    let fullUrl = `https://dsns.ischool.com.tw/${this.dsns}`;

    if (this.dsns.startsWith('http://') || this.dsns.startsWith('https://')) {
      fullUrl = this.dsns;
    }

    try {
      const req = Converter.toXml(env, 'Envelope');
      const xmlrsp = await http.post(fullUrl, req, { responseType: 'text' }).toPromise();
      const rsp = Converter.toJson(xmlrsp);
      const { Code = '', Message = '' } = Converter.value(rsp, 'Envelope/Header/Status') || {};

      if (Code === '0') {
        return rsp.Envelope.Body as T;
      } else {
        if (!rsp.Envelope) {
          throw new Error('DSNS 不存在或未知錯誤。');
        } else {
          const error = new DSAError(Message);
          error.response = rsp.Envelope;
          error.url = fullUrl;
          throw error;
        }
      }

    } catch (error) {
      if (error instanceof DSAError) {
        throw error;
      } else {
        throw new DSAError(`嚴重錯誤：${fullUrl}, 訊息：${error.message}`);
      }
    }
  }

  private createRequest(contract: string, service: string, body: any) {

    const Body = body;

    const env: Envelope = {
      Header: {
        TargetContract: this.contract,
        TargetService: service,
        SecurityToken: this.getSecurityToken()
      },
      Body
    };

    return env;
  }

  private getSecurityToken(): SecurityToken {
    if (!this.sessionId || !this.enabledSession) {
      return {
        '@Type': 'Basic',
        UserName: this.userid,
        Password: this.password
      };
    } else {
      return {
        '@Type': 'Session',
        'SessionID': this.sessionId,
      };
    }
  }
}

export class DSAError extends Error {

  constructor(message?: string) {
    super(message); // 'Error' breaks prototype chain here
    Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
  }

  public url: string;

  public response: any;
}
