
/**
<Envelope>
	<Header>
		<TargetService>DS.Base.Connect</TargetService>
		<TargetContract>admin</TargetContract>
		<SecurityToken Type="Basic">
			<UserName>admin</UserName>
			<Password>這是密秘!!</Password>
		</SecurityToken>
	</Header>
	<Body>
		<RequestSessionID />
	</Body>
</Envelope>
 */
export interface Envelope {

  Header: Header;

  Body?: any;
}

export interface Header {

  [x: string]: any;

  TargetService: string;

  TargetContract: string;

  SecurityToken: SecurityToken;
}

export interface SecurityToken {

  [x: string]: any;

  UserName?: string;

  Password?: string;
}

export class Converter {

  public static toXml(obj: any, rootName: string) {
    const xmlparser = parseXml.CreateParser();

    return xmlparser.parse(obj, rootName);
  }

  public static toJson(xml: string) {
    return xml2json.parse(xml);
  }

  public static value(obj, path) {

    if (!path) {
      return undefined;
    }

    if (typeof (obj) === 'string') {
      return undefined;
    }

    let current = obj;
    for (const p of path.split('/')) {
      current = current[p];

      if (!current) {
        current = undefined;
        break;
      }
    }

    return current;
  }

  public static array(obj, path) {
    if (path) {
      return [].concat(Converter.value(obj, path) || []);
    } else {
      return [].concat(obj || []);
    }
  }
}
