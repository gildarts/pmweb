import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypescriptComponent } from './typescript/typescript.component';

/*
所有程式必需要放在 assets 裡面，因為 manaco.bundle.js 並沒有包含所有程式碼，
只有必要的，真正執行時會即時下載需要的。而且 /assets/manaco-bundle 這路徑
在打包時就要設定正確，否則仍然會無法使用。
*/
import '../../assets/monaco-bundle/monaco.bundle.js';
import { SqlComponent } from './sql/sql.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TypescriptComponent, SqlComponent],
  exports: [
    TypescriptComponent,
    SqlComponent
  ]
})
export class EditorModule { }
