import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-sql',
  templateUrl: './sql.component.html',
  styleUrls: ['./sql.component.css']
})
export class SqlComponent implements OnInit {

  editor: monaco.editor.IStandaloneCodeEditor;

  constructor() { }

  @ViewChild('container') container: ElementRef;

  ngOnInit() {
    this.editor = monaco.editor.create(this.container.nativeElement, {
      value: [`select student.id, name, birthdate, id_number, class.class_name, student.seat_no
from student join class on student.ref_class_id = class.id
order by class.class_name, student.seat_no`].join('\n'),
      language: 'sql',
      // theme: 'vs-dark',
      fontSize: 17,
      automaticLayout: true,
      minimap: { enabled: false }
    });
  }

  public get selectedText() {
    return this.editor.getModel().getValueInRange(this.editor.getSelection());
  }

  public get text() {
    return this.editor.getModel().getValue();
  }

}
