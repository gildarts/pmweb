import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-typescript',
  templateUrl: './typescript.component.html',
  styleUrls: ['./typescript.component.css']
})
export class TypescriptComponent implements OnInit {

  editor: monaco.editor.IStandaloneCodeEditor;

  constructor() { }

  @ViewChild('container') container: ElementRef;

  ngOnInit() {
    this.editor = monaco.editor.create(this.container.nativeElement, {
      value: [`// UDS 工具開發中 A_A

function say() {
  console.log('hello');
}
`].join('\n'),
      language: 'typescript',
      theme: 'vs-dark',
      fontSize: 18,
      automaticLayout: true,
    });
  }

  updateEditor() {
    this.editor.layout();
  }
}
