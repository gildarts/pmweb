import { Component, OnInit } from '@angular/core';
import { WindowRefService } from '../../core/window-ref.service';

@Component({
  selector: 'app-auth-callback',
  templateUrl: './auth-callback.component.html',
  styleUrls: ['./auth-callback.component.css']
})
export class AuthCallbackComponent implements OnInit {

  constructor(
    private winRef: WindowRefService
  ) { }

  ngOnInit() {
    const ref = this.winRef.getNativeWindow();
    ref.close();
  }

}
