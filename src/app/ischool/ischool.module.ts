import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IschoolRoutingModule } from './ischool-routing.module';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';

@NgModule({
  imports: [
    CommonModule,
    IschoolRoutingModule
  ],
  declarations: [AuthCallbackComponent]
})
export class IschoolModule { }
