import { Component, OnInit } from '@angular/core';
import { AuthService } from './core/auth.service';
import { Router } from '@angular/router';
import { GoogleUserData } from './core/google-user-data';
import { DSAService } from './dsa';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent implements OnInit {

  loading = true;

  userInfo: GoogleUserData;

  // dsa_list = [
  //   'dev.sh_d',
  //   'demo.ischool.h'
  // ];

  constructor(
    private auth: AuthService,
    private dsa: DSAService,
    private router: Router
  ) { }

  async ngOnInit() {
    const logined = await this.auth.authenticated.toPromise();

    if (!logined) {
      this.router.navigate(['/login']);
    } else {
      this.userInfo = await this.auth.userInfo.toPromise();
      this.loading = false;
    }
  }

  async logout() {
    await this.auth.logout().subscribe({
      next: (success) => {
        if (success) {
          this.router.navigate(['/login']);
        } else {
          alert('logout fail!');
        }
      },
      error: (err) => {
        alert(err.message);
      }
    });
  }

  async registerNew() {
    const reg = await this.dsa.register().toPromise();

    if (reg) {
      const rsp = await this.dsa.saveRegistration(reg).toPromise();
    } else {
      console.log('nothing...');
    }
  }
}
