import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { JSONComponent } from './json/json.component';
import { DSAErrorComponent } from './dsa-error/dsa-error.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(
    private dialog: MatDialog
  ) { }

  public json(json: any) {
    return this.dialog.open(JSONComponent, { data: json });
  }

  public dsa(error: Error) {
    return this.dialog.open(DSAErrorComponent, { data: error });
  }
}
