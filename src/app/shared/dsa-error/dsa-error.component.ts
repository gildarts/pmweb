import { Component, OnInit, Optional, Inject, Input } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dsa-error',
  templateUrl: './dsa-error.component.html',
  styleUrls: ['./dsa-error.component.css']
})
export class DSAErrorComponent implements OnInit {

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  @Input() error: Error;

  get errorData() {
    if (this.error) {
      return this.error;
    } else {
      return this.data;
    }
  }

  get response() {
    return this.errorData.response || {};
  }

  get url() {
    return this.errorData.url || '';
  }

  get header() {
    return this.response.Header || {};
  }

  get dsfault(): Fault {
    const { DSFault = {} } = this.header;
    return DSFault.Fault || {};
  }

  get status(): Status {
    return this.header.Status || {};
  }

  get processTime(): string {
    return this.header.ProcessTime;
  }

  get version(): string {
    return this.header.Version;
  }

  ngOnInit() {
  }
}

interface Status {
  Code: string;
  Message: string;
}

interface Fault {
  Source: string;
  Code: string;
  Message: string;
  Detail: string;
}
