import { Component, OnInit, Inject, Input, Optional } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-json',
  templateUrl: './json.component.html',
  styleUrls: ['./json.component.css']
})
export class JSONComponent implements OnInit {

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  @Input() json: any;

  get jsonData() {
    if (this.json) {
      return this.json;
    } else {
      return this.data;
    }
  }

  ngOnInit() {
  }

}
