import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from './loading/loading.component';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { JSONComponent } from './json/json.component';
import { DSAErrorComponent } from './dsa-error/dsa-error.component';

const sharedModuels = [
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatIconModule,
  MatMenuModule,
  MatButtonModule,
  MatSidenavModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  FormsModule,
  ReactiveFormsModule,
  MatTableModule,
  MatListModule,
  MatCardModule,
  MatAutocompleteModule
];

@NgModule({
  imports: [
    CommonModule,
    ...sharedModuels
  ],
  exports: [
    LoadingComponent,
    DSAErrorComponent,
    JSONComponent,
    ...sharedModuels
  ],
  entryComponents: [
    JSONComponent,
    DSAErrorComponent
  ],
  declarations: [LoadingComponent, JSONComponent, DSAErrorComponent]
})
export class SharedModule { }
