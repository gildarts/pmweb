import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {

  constructor(
    private auth: AuthService
  ) { }

  async ngOnInit() {
    // this.auth.reset();
    // const user = await this.auth.authenticated.toPromise();
    // console.log('loginSuccess: ', user);
    // console.log('userInfo: ', await this.auth.userInfo.toPromise());
  }

  logout() {
    // this.auth.logout().toPromise();
  }
}
