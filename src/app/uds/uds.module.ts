import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorModule } from '../editor/editor.module';
import { ServiceComponent } from './service/service.component';
import { UdsRoutingModule } from './uds-routing.module';

@NgModule({
  imports: [
    CommonModule,
    EditorModule,
    UdsRoutingModule
  ],
  declarations: [ServiceComponent]
})
export class UdsModule { }
