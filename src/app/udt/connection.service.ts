import { Injectable } from '@angular/core';
import { Connection } from '../dsa';
import { from } from 'rxjs';

let singleton: Connection;

@Injectable({providedIn: 'root'})
export class ConnectionService {

  constructor() {
  }

  public send<T>(serviceName: string, body?: any) {
    return from(singleton.send<T>(serviceName, body));
  }
}

export function provideConnection(conn: Connection) {
  singleton = conn;
}
