import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DDLService {

  constructor(
    private conn: ConnectionService
  ) { }

  public listTableNames() {
    const srvName = 'UDTService.DDL.ListTableNames';
    return this.conn.send<any>(srvName).pipe(
      map(v => [].concat(v.Response.TableName || []))
    );
  }
}
