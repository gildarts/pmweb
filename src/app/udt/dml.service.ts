import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { map } from 'rxjs/operators';
import { XmlTable } from './xml-table';

@Injectable({
  providedIn: 'root'
})
export class DMLService {

  constructor(
    private conn: ConnectionService
  ) { }

  /**
   * 以 SQL 查詢資料
   */
  public query(cmd: string) {
    const srvName = 'UDTService.DML.Query';
    return this.conn.send<any>(srvName, {
      Request: {
        SQL: cmd
      }
    }).pipe(
      map(v => v.Response),
      map(XmlTable.toJson)
    );
  }

  /**
   * 以 SQL Command 變更資料
   */
  public command(cmd: string) {
    const srvName = 'UDTService.DML.Command';
    return this.conn.send<any>(srvName, {
      Request: {
        Command: cmd
      }
    }).pipe(
      map(v => v.Response)
    );
  }
}
