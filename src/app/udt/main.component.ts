import { Component, OnInit } from '@angular/core';
import { DSAService, Connection } from '../dsa';
import { DSARegistration } from '../dsa/dsa-registration';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { DialogService } from '../shared/dialog.service';
import { HttpClient } from '@angular/common/http';
import { provideConnection } from './connection.service';
// import { GridApi } from 'ag-grid-community';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  loading = true;
  registration: DSARegistration;
  conn: Connection;

  // private gridApi: GridApi;

  // columnDefs = [
  //   { headerName: 'Make', field: 'make' },
  //   { headerName: 'Model', field: 'model' },
  //   { headerName: 'Price', field: 'price' }
  // ];

  // rowData = [
  //   { make: 'Toyota', model: 'Celica', price: 35000 },
  //   { make: 'Ford', model: 'Mondeo', price: 32000 },
  //   { make: 'Porsche', model: 'Boxter', price: 72000 }
  // ];

  constructor(
    private dsa: DSAService,
    private route: ActivatedRoute,
    private dialog: DialogService,
    private http: HttpClient
  ) { }

  async ngOnInit() {
    this.route.paramMap.pipe(
      map(v => v.get('dsns')),
      switchMap(v => this.dsa.get({ dsns: v }))
    )
      .subscribe(async v => {
        this.registration = v;
        await this.initdsa();
      }, error => {
        this.dialog.json(error);
      });
  }

  async initdsa() {
    const { dsns, login_user, login_password } = this.registration;
    const conn = new Connection(dsns, 'admin', login_user, login_password);

    await conn.connect(this.http);
    this.registration.connection = conn;
    this.conn = conn;
    provideConnection(conn);
    this.loading = false;
  }

  // copy(event: KeyboardEvent) {

  //   if (event.metaKey && event.key === 'c') {
  //     console.log(this.gridApi.getSelectedRows());
  //     console.log(this.gridApi.getSelectedNodes());

  //     const val = this.gridApi.getValue(this.gridApi.getFocusedCell().column, this.gridApi.getSelectedNodes()[0]);
  //     console.log(val);

  //   }
  // }

  // gridReady(event) {
  //   this.gridApi = event.api;
  //   console.log(event);
  // }
}
