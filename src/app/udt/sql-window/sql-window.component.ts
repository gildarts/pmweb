import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ConnectionService } from '../connection.service';
import { DDLService } from '../ddl.service';
import { DMLService } from '../dml.service';
import { DialogService } from '../../shared/dialog.service';
import { GridApi } from 'ag-grid-community';
import { SqlComponent } from 'src/app/editor/sql/sql.component';

@Component({
  selector: 'app-sql-window',
  templateUrl: './sql-window.component.html',
  styleUrls: ['./sql-window.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SqlWindowComponent implements OnInit {

  response: any;

  dataSource = [];
  displayedColumns = [];
  error = {};

  private gridApi: GridApi;

  columnDefs = [];

  rowData = [];

  constructor(
    private ddl: DDLService,
    private dml: DMLService,
    private dialog: DialogService
  ) { }

  @ViewChild('t_sql') sql: SqlComponent;

  async ngOnInit() {
  }

  public async executeSql() {
    try {
      const rsp = await this.dml.query(this.getCommand()).toPromise();
      this.response = rsp;
      this.dataSource = rsp.records;
      this.displayedColumns = [...rsp.columns.keys()];
      this.rowData = this.dataSource;
      this.columnDefs = this.displayedColumns.map(v => {
        return { headerName: v, field: v };
      });
    } catch (error) {
      this.dialog.dsa(error);
    }
  }

  private getCommand(): string {
    if (this.sql.selectedText) {
      return this.sql.selectedText;
    } else {
      return this.sql.text;
    }

  }

  gridReady(event) {
    this.gridApi = event.api;
  }
}
