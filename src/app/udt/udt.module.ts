import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UdtRoutingModule } from './udt-routing.module';
import { MainComponent } from './main.component';
import { SqlWindowComponent } from './sql-window/sql-window.component';
import { SharedModule } from '../shared/shared.module';
import { EditorModule } from '../editor';
import { AgGridModule } from 'ag-grid-angular';

import 'ag-grid-enterprise';

@NgModule({
  imports: [
    CommonModule,
    UdtRoutingModule,
    SharedModule,
    EditorModule,
    AgGridModule.withComponents([])
  ],
  declarations: [MainComponent, SqlWindowComponent]
})
export class UdtModule { }
