
export class XmlTable {

  public static toJson(response) {
    const { Metadata, Record } = response;
    const columns = XmlTable.toField(Metadata.Column);
    const ensureRecords = [].concat(Record || []);
    const recordsArray: any[][] = ensureRecords.map(v => v.Column);

    const records = recordsArray.map(arr => {
      return XmlTable.fieldsToRecord(arr, columns);
    }) as DataRecord[];

    const nameMap = new Map<string, Field>();
    for (const column of columns.values()) {
      nameMap.set(column.name, column);
    }

    return { columns: nameMap, records };
  }

  private static fieldsToRecord(arr: any[], columns: Map<string, Field>) {
    const record: any = {};
    for (const { Index, '@text': text } of arr) {
      const orgName = columns.get(Index).name;
      let finalName = orgName;
      let duplicate = 1;
      while (record[finalName]) {
        finalName = `${orgName}_${duplicate++}`;
      }
      record[finalName] = text || '';
    }
    return record;
  }

  private static toField(fields: any[]) {
    const entries = fields.map(v => XmlTable.toFieldEntry(v)) as [[string, Field]];
    const map = new Map<string, Field>(entries);

    return map;
  }

  private static toFieldEntry(v: any): any[] {
    return [v.Index, { name: v.Field, index: v.Index, type: v.Type }];
  }
}

export interface Field {
  name: string;
  index: string;
  type: string;
}

export interface DataRecord {
  [x: string]: any;
}
